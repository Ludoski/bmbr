package com.company.bmbr.services;

import com.company.bmbr.helpers.Help;
import com.company.bmbr.models.ShortUrl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PayloadService {

  private static final ObjectMapper objectMapper = new ObjectMapper();

  public static String createPayload(int bomberType) {
    switch (bomberType) {
      case 0:
        return objectToString(ShortUrl.getPayload());
      case 1:
        return "";
      default:
        System.out.println("ERROR: Invalid bomber type.");
        Help.showHelp();
    }

    return null;
  }

  private static String objectToString(Object model) {
    try {
      return objectMapper.writeValueAsString(model);
    } catch (JsonProcessingException e) {
      System.out.println("ERROR: Unable to create payload." + e);
      Help.showHelp();
    }

    return null;
  }

}
