package com.company.bmbr.services;

import com.company.bmbr.enums.HttpMethod;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.CompletableFuture;

public class SendRequestService {

  private static final HttpClient client = HttpClient.newHttpClient();

  public static void send(HttpMethod httpMethod, String targetUrl, String payload, boolean silent) {
    HttpRequest request = HttpRequest.newBuilder()
            .uri(URI.create(targetUrl))
            .header("Content-Type", "application/json")
            .method(String.valueOf(httpMethod), HttpRequest.BodyPublishers.ofString(payload))
            //.POST(HttpRequest.BodyPublishers.ofString(payload))
            .build();

    if (!silent) {
      System.out.println();
      System.out.println("Request: " + request.method() + " " + request.uri() + " " + payload);
      System.out.print("Response: ");

      HttpResponse response = client.sendAsync(request, HttpResponse.BodyHandlers.ofString()).join();
      System.out.println(response.statusCode() + " " + response.body());
    } else {
      client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
              .thenApply(HttpResponse::body)
              .join();
    }

  }

}
