package com.company.bmbr.services;

import com.company.bmbr.enums.HttpMethod;
import com.company.bmbr.helpers.ArgsParser;

public class BomberService {

  private static HttpMethod httpMethod;
  private static String targetUrl;
  private static int bomberType;
  private static long numberOfRequests;
  private static boolean silent;


  public static void start() {
    getParams();
    bomb();
  }

  private static void getParams() {
    httpMethod = ArgsParser.getHttpMethod();
    targetUrl = ArgsParser.getTargetUrl();
    bomberType = ArgsParser.getBomberType();
    numberOfRequests = ArgsParser.getNumberOfRequests();
    silent = ArgsParser.getSilent();
  }

  private static void bomb() {
    for (long i = 0; i < numberOfRequests; i++) {
      String payload = PayloadService.createPayload(bomberType);
      SendRequestService.send(httpMethod, targetUrl, payload, silent);
    }
  }

}
