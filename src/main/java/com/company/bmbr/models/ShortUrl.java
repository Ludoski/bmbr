package com.company.bmbr.models;

import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Builder
@Getter
public class ShortUrl {

  private String url;

  public static ShortUrl getPayload() {
    return ShortUrl.builder()
            .url(String.valueOf(UUID.randomUUID()))
            .build();
  }

}
