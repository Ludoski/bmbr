package com.company.bmbr.enums;

public enum HttpMethod {
  GET, POST, PATCH, PUT, DELETE
}
