package com.company.bmbr.helpers;

import com.company.bmbr.enums.HttpMethod;

import java.util.HashMap;
import java.util.Map;

public class ArgsParser {

  private static String[] args;
  private static Map<String, String> params;

  public static void parseArgs(String[] arguments) {
    args = arguments;
    verifyFormat();
    createParams();
  }

  // Check for valid key-value format
  private static void verifyFormat() {
    if (args.length == 0 || args.length % 2 != 0) {
      System.out.println("ERROR: Invalid arguments. Arguments must be in key-value format.");
      Help.showHelp();
    }
  }

  // Create params from args
  private static void createParams() {
    params = new HashMap<>();
    for (int i = 0; i < args.length - 1; i++) {
      params.put(args[i], args[i + 1]);
    }
  }

  // Get http method
  public  static HttpMethod getHttpMethod() {
    HttpMethod httpMethod = null;

    try {
      httpMethod = HttpMethod.valueOf(params.get("-m"));
    } catch (IllegalArgumentException e) {
      System.out.println("ERROR: Http method (-m) invalid.");
      Help.showHelp();
    }

    return httpMethod;
  }

  // Get target url
  public static String getTargetUrl() {
    return params.get("-u");
  }

  // Get bomber type value
  public static int getBomberType() {
    int bomberType = -1;

    try {
      bomberType = Integer.parseInt(params.get("-b"));
    } catch (NumberFormatException e) {
      System.out.println("ERROR: Bomber type (-b) must be number.");
      Help.showHelp();
    }

    return bomberType;
  }

  // Get number of requests value
  public static long getNumberOfRequests() {
    long numberOfRequests = 0;

    try {
      numberOfRequests = Integer.parseInt(params.get("-n"));
    } catch (NumberFormatException e) {
      System.out.println("ERROR: Number of requests value (-n) must be number.");
      Help.showHelp();
    }

    return numberOfRequests;
  }

  // Get silent value
  public static boolean getSilent() {
    return Boolean.parseBoolean(params.get("-s"));
  }

}
