package com.company.bmbr.helpers;

public class Help {

  public static void showHelp() {
    System.out.println();
    System.out.println("Command line arguments:");
    System.out.println("-m: Http method (GET, POST, PATCH, PUT, DELETE)");
    System.out.println("-u: Target url");
    System.out.println("-b: Bomber type");
    showBomberTypes();
    System.out.println("-n: Number of requests to send");
    System.out.println("-s: Silent");
    showSilentTypes();

    System.out.println();
    System.out.println("Bmbr usage example:");
    System.out.println("java -jar bmbr.jar -m GET -u http://localhost:8080 -b 0 -n 10 -s true");
    System.out.println();

    System.exit(0);
  }

  private static void showBomberTypes() {
    System.out.println("\t 0: Shortener - Create random slug");
    System.out.println("\t 1: Key - Get status");
  }

  private static void showSilentTypes() {
    System.out.println("\t true");
    System.out.println("\t false");
  }

}
