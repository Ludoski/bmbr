package com.company.bmbr;

import com.company.bmbr.helpers.ArgsParser;
import com.company.bmbr.services.BomberService;

public class Application {

  public static void main(String[] args) {
    // For testing purposes. Comment before build.
    //args = new String[] {"-m", "GET", "-u", "http://localhost:2550/key/util/status", "-b", "0", "-n", "20", "-s", "true1"};

    // Parse args
    ArgsParser.parseArgs(args);

    // Start bombing
    System.out.println("Bombing... Please wait...");
    long startTime = System.currentTimeMillis();

    BomberService.start();

    long endTime = System.currentTimeMillis();
    System.out.println();
    System.out.println("Bombing completed.");
    System.out.println("Elapsed " + (endTime - startTime) / 1000F + " seconds.");
  }

}
